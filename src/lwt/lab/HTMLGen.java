package lwt.lab;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lwt.lab.html.utils.HTMLUtils;

/**
 * Servlet implementation class HTTPGen
 */
@WebServlet("/HTMLGen")
public class HTMLGen extends HttpServlet {
    private static final long serialVersionUID = 1L;
    protected static final StringBuilder bodyBuilder = new StringBuilder();
    protected static final String TEXTAREA_NAME_LABEL = "name";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public HTMLGen() {
	super();
	String form = generateForm();
	bodyBuilder.append(form);
    }

    protected String generateForm() {
	String form = HTMLUtils.createTextareaForm(TEXTAREA_NAME_LABEL, "HTMLGen", "post");
	return form;
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	response.getWriter().append(HTMLUtils.titleBodyHTML("HTML Gen", bodyBuilder.toString()));
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	addInsertedHTML(request.getParameter(TEXTAREA_NAME_LABEL));
	doGet(request, response);
    }

    protected void addInsertedHTML(String content) {
	bodyBuilder.append(content);
    }

}
