package lwt.lab;

import javax.servlet.annotation.WebServlet;

import lwt.lab.html.utils.HTMLUtils;
import lwt.lab.utils.FilterUtils;

/**
 * Servlet implementation class FilteredHTMLGen
 */
@WebServlet("/FilteredHTMLGen")
public class FilteredHTMLGen extends HTMLGen {
    private static final long serialVersionUID = 1L;

    /*
     * (non-Javadoc)
     * 
     * @see lwt.lab.HTMLGen#generateForm()
     */
    @Override
    protected String generateForm() {
	String form = HTMLUtils.createTextareaForm(TEXTAREA_NAME_LABEL, "FilteredHTMLGen", "post");
	return form;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * lwt.lab.HTMLGen#addInsertedHTML(javax.servlet.http.HttpServletRequest)
     */
    @Override
    protected void addInsertedHTML(String content) {
	super.addInsertedHTML(FilterUtils.filter(content));
    }

}
